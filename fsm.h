#ifndef _FSM_H_
#define _FSM_H_

#ifndef FSM_SIZE_TYPE
# ifdef size_t
#   define FSM_SIZE_TYPE size_t
# else
#   define FSM_SIZE_TYPE unsigned long long
# endif
#endif

#ifndef FSM_BOOL_TYPE
# ifdef bool
#   define FSM_BOOL_TYPE bool
# else
#   define FSM_BOOL_TYPE unsigned char
# endif
#endif

#ifndef FSM_FALSE
# ifdef false
#   define FSM_FALSE false
# else
#   define FSM_FALSE ((FSM_BOOL_TYPE)0)
# endif
#endif

#ifndef FSM_TRUE
# ifdef true
#   define FSM_TRUE true
# else
#   define FSM_TRUE ((FSM_BOOL_TYPE)1)
# endif
#endif

#define FSM_FORWARD_STATEMACHINE(fsm_name, state_type, event_type)      \
  struct fsm_name##_transition;                                         \
  struct fsm_name;                                                      \
  typedef void (*fsm_name##callback) (struct fsm_name *, event_type, state_type, state_type); \
  void fsm_name##_init(struct fsm_name *, state_type, const struct fsm_name##_transition *transitions, const FSM_SIZE_TYPE); \
  FSM_BOOL_TYPE fsm_name##_consume(struct fsm_name *, event_type);      \


#define FSM_DECLARE_STATEMACHINE(fsm_name, state_type, event_type)      \
  FSM_FORWARD_STATEMACHINE(fsm_name, state_type, event_type)            \
  struct fsm_name##_transition {                                        \
    event_type event;                                                   \
    state_type source;                                                  \
    state_type target;                                                  \
    fsm_name##callback transit;                                         \
  };                                                                    \
  struct fsm_name {                                                     \
    state_type state;                                                   \
    FSM_SIZE_TYPE num_transitions;                                      \
    const struct fsm_name##_transition *transitions;                    \
  };                                                                    \
  void fsm_name##_init(struct fsm_name *, state_type, const struct fsm_name##_transition *, const FSM_SIZE_TYPE); \
  FSM_BOOL_TYPE fsm_name##_consume(struct fsm_name *, event_type);      \


#define FSM_DEFINE_STATEMACHINE(fsm_name, state_type, event_type)       \
  FSM_DECLARE_STATEMACHINE(fsm_name, state_type, event_type)            \
  void fsm_name##_init(struct fsm_name *fsm, state_type state, const struct fsm_name##_transition *transitions, const FSM_SIZE_TYPE num_transitions) { \
    fsm->state           = state;                                       \
    fsm->num_transitions = num_transitions;                             \
    fsm->transitions     = transitions;                                 \
  }                                                                     \
  FSM_BOOL_TYPE fsm_name##_consume(struct fsm_name *fsm, event_type event) { \
    for (const struct fsm_name##_transition *ptr = fsm->transitions; ptr != fsm->transitions + fsm->num_transitions; ++ptr) { \
      if ((ptr->source == fsm->state) && (ptr->event == event)) {       \
        fsm->state = ptr->target;                                       \
        ptr->transit(fsm, ptr->event, ptr->source, ptr->target);        \
        return FSM_TRUE;                                                \
      }                                                                 \
    }                                                                   \
    return FSM_FALSE;                                                   \
  }                                                                     \

#define FSM_NUM_TRANSITIONS(fsm_name, transitions) (sizeof(transitions)/sizeof(struct fsm_name##_transition))

#define FSM_INIT(fsm_name, fsm, state, transitions) (fsm_name##_init(&fsm, state, transitions, FSM_NUM_TRANSITIONS(fsm_name, transitions)))

#define FSM_CONSUME(fsm_name, fsm, event) (fsm_name##_consume(&fsm, event))

#endif // _FSM_H_
