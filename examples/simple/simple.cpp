
#include "fsm.hpp"

typedef uint8_t event_t;
typedef uint8_t state_t;
typedef fsm::statemachine<event_t, state_t> myfsm_t;

myfsm_t myFsm {
  .state = 0, // starting in this state
  .transitions = {

    // transition 1
    {
      .filter   = myfsm_t::match({ 1, 2, 3 }), // matching events
      .source   = 0,                           // source state
      .target   = 1,                           // destination state
      .transit = [] (const event_t& evt, const state_t& src, const state_t& dst) {

        // do stuff during transition...
        
      }
    },

    // transition 2
    {
      .filter   = myfsm_t::match({ 2, 3, 4 }),
      .source   = 1,
      .target   = 0,
      // multiple callbacks
      .transit = myfsm_t::series({
        [] (const event_t& evt, const state_t& src, const state_t& dst) {

          // do stuff during transition...

        },
        [] (const event_t& evt, const state_t& src, const state_t& dst) {

          // do stuff during transition...

        }
      })
    },
  },
  // for every transition
  .transit = [] (const event_t& evt, const state_t& src, const state_t& dst) {

    // do stuff during any transition...

  }
}

void setup()
{

  // myFsm.state => 0
  myFsm(1); // => true,  myFsm.state => 1
  myFsm(2); // => true,  myFsm.state => 0
  myFsm(0); // => false, myFsm.state => 0

}

void loop()
{

}
