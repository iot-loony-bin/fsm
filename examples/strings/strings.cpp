
#include <Arduino.h>

#include "fsm.hpp"

#include <string>
#include <sstream>

typedef std::string event_t;
typedef std::string state_t;
typedef fsm::statemachine<event_t, state_t> myfsm_t;

myfsm_t myFsm {
  .state = "OFF", // starting in this state
  .transitions = {

    {
      .filter   = myfsm_t::match({ "on", "toggle" }), // matching events
      .source   = "OFF",                              // source state
      .target   = "ON",                               // destination state
      .transit = [] (const event_t& evt, const state_t& src, const state_t& dst) {
        // switch on light...
        Serial.println("LIGHT IS ON");
      }
    },

    {
      .filter   = myfsm_t::match({ "off", "toggle" }),
      .source   = "ON",
      .target   = "OFF",
      .transit = [] (const event_t& evt, const state_t& src, const state_t& dst) {
        // switch off light...
        Serial.println("LIGHT IS OFF");
      }
    },

  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println("LIGHT IS OFF");
}

std::ostringstream serialLineBuffer(256);

void loop()
{
  int ch;

  for (int available = Serial.available(); available > 0; available--) {
    ch = Serial.read();
    switch (ch) {
    
    case '\n':
      if (myFsm(serialLineBuffer.str())) {
        Serial.print("Event '");
        Serial.print(serialLineBuffer.str().c_str());
        Serial.println("' has been consumed.")
      } else {
        Serial.print("Event '");
        Serial.print(serialLineBuffer.str().c_str());
        Serial.println("' has been ignored.")
      }
      serialLineBuffer = std::ostringstream(256);
      break;
    
    default:
      serialLineBuffer.put(ch);
      break;
      
    }
    Serial.write(ch);
  }
}
