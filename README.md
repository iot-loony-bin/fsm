# fsm - Flexible header-only Finite-state machine

This library provides a configurable state machine template for C++. It can be used
to define a state machine using states, events and transitions.

## What are state machines good for?

A finite-state machine is always in a single state and consumes events that are
generated somewhere (e.g. a sensor reading becomes available or there is data
available on the serial line). Whenever the statemachine receives an event, a
user defined transition table is queried. A transition is composed of a source
state, a target state and an event filter. If there is a matching transition it
is applied, which means if the state machine currently is in the source state
and the event filter matches, user defined callback handlers are called and the
state machine's current state is changed to the transition's target state.

Finite-state machines are a good way to design very stable code that is suprisingly
error resistant. By allowing only well defined transitions for each state every
undefined event can be handled as error by the user or, depeding on the use case,
simply be discarded.

## How to design a state machine?

State machines can be designed in graphical editors or using a pen and paper.
Graphical representations are composed of nodes which represent the states (using
circles or larger boxes) and directed edges between nodes which represent possible
transitions for particular events.

## A simple example

_Example of an __ON__/__OFF__ controller that can handle the events __switch on__,
__switch off__ and __toggle switch___

![finite state machine](fsm.png)

This statemachine always starts in the __OFF__ state. In this example if a
__switch on__ event occurs and the machine already is in the __ON__ state, that
event is ignored. The __toggle switch__ event is bidirectional and can be used in
both states.
